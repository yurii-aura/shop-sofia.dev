window.addEventListener("scroll", function() {
    const mediaQuery = window.matchMedia( '( min-width: 1200px )' )
    const navArea = document.querySelector(".pc-head");
    if (mediaQuery.matches) {
        if (window.pageYOffset > 50) {
            navArea.classList.add("hide-top-header-part");
        } else {
            navArea.classList.remove("hide-top-header-part");
        }
    }
});