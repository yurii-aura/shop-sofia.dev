$('.carousel_content').slick({
    // lazyLoad: 'ondemand',
    slidesToShow: 8,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    prevArrow: '<div class="carousel_control carousel_control_left"><div><span>&#8249;</span></div></div>',
    nextArrow: '<div class="carousel_control carousel_control_right"><div><span>&#8250;</span></div></div>',
    autoplay: false,
    autoplaySpeed: 5000,
    responsive: [
        {
            breakpoint: 1025,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 500,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
            }
        },
    ]
});